1. Настроить hostname
2. Настроить сетевые интерфейсы
auto enp6s0
iface enp6s0 inet static
address 2.2.1.2/29
gateway 2.2.1.1
post-up /etc/gre.up

auto enp1s0
iface enp1s0 inet static
address 192.168.3.1/24


/etc/gre.up
ip tunnel add tun0 mode gre local 2.2.1.2 remote 1.2.1.2 ttl 64
ip addr add 10.5.5.1/30 dev tun0
ip link set up tun0


./gre.up
reboot
3. /etc/nftables.conf
table inet filter {
		chain input {
		type filter hook input priority 0;
		policy drop;
		iif lo accept
		iif enp1s0 accept
		iif tun0 accept
		ct state established, related accept
		tcp dport { 22, 8025 } ct state new accept
		udp dport { 500, 4500 } accept
		ip protocol { ah, esp, gre } accept
		counter drop
	}
	chain forward {
		type filter hook forward priority 0;
		}
	chain output {
		type filter hook ouput priority 0;
	}
}
table ip nat {
 
		chain prerouting {
			type nat hook prerouting priority 0;
		}
		chain postrouting {
			type nat hook postrouting priority 0;
			oif enp6s0 masquerade;
		}
}

4. установка frr
apt install frr (br-rtr cr-rtr)

frr version 7.5.1
frr defaults traditional
hostname br-rtr.rea2023.ru
log syslog informational
no ipv6 forwarding
service integrated-vtysh-config
!
interface tun0
 ip ospf network broadcast
!
router ospf 
 network 10.5.5.0/30
 network 192.168.2.0/24 area 0
!
line vty
!

vtysh do sh ip ospf nei

на бр и цр не забыть включить форвардинг!!!
проверка - sysctl -p


5.dhcp
apt update 
apt install isc-dhcp-server
nano /etc/dhcpd/dhcpd.conf

option domain-name "rea2023.ru";
option domain-name-servers 192.168.2.51;

default-lease-time 32400;
max-lease-time 604800;

ddns-updates on;
ddns-update-style interim;
update-static-leases on;
subnet 192.168.2.0 netmask 255.255.255.0 {
range 192.168.2.50 192.168.2.100;
option routers 192.168.2.1;
}
host br-srv1 {
hardware ethernet mak;
fixed-address 192.168.2.51;
}

systemctl restart isc-dhcp-server
проверка dhclient - v

6. dns на br-srv1
cd /opt/rea2023
chmod 777 /opt/rea2023
soa rea2023.ru. root.rea2023.ru
не забываем менять числ попыток
        IN NS   br-srv1.rea2023.ru
br-srv1 IN A 192.168.2.51
@       IN A 192.168.2.51
cr-srv  IN A 192.168.3.10
test IN A 2.2.2.2
br-cli
br-srv2
web
cr-rtr

в файле rea2023.ex.ru.db - отдельный файл в бр кли
то же самое
только test IN A 1.1.1.1

CD /etc/bind
nano named.conf.options
directory "/opt/rea2023";
forwarders {
77.88.8.8;
}
dnssec-validation auto;
}


nano named.conf.default-zones
acl "br-cli" {192.168.2.52;};
view "in" {
match-clients {"br-cli";};
zone "rea2023.ru" {
	type master;
	file "/opt/rea2023/rea2023.ex.ru.db";
	allow-update {any;};
};
zone "168.192.in-addr.arpa" {
		type master;
		file "/opt/rea2023/168.192.in-addr.arpa.db";
};
zone "itnsa.lab" {
		type forward;
		forwarders { 10.113.38.100; };
};
};
acl "another" {any;};
view "int" {
match-clients { "another";};
zone "rea2023.ru" {
	type master;
	file "/opt/rea2023/rea2023.ru.db";
	allow-update {any;};
};
zone "itnsa.lab" {
	type forward;
	forwarders { 10.113.38.100; };
};
};

5. zabbix

docker run --name zabbix-appliance -t -p 10051:10051 -p 80:80 -d zabbix/zabbix-appliance:latest
docker rm -f zabbix-appliance
зайти 0.0.0.0:80
Admin
zabbix

в днс добавить zabbix.rea2023.ru A адрес
если не работает то добавляй в /etc/hosts
6. перемещаемые профили
apt install nfs-server
/etc/exports
/srv/homes   hostname1(rw,sync,no_subtree_check) hostname2(ro, sync, no_subtree_check)
/srv/nfs4    gss/krb5i(rw,sync,fsid=0,crossmnt,no_subtree_check)
/srv/nfs4/homes  gss/krb5i(rw,sync,no_subtree_check)

/srv/homes  192.168.0.0/16(rw,sync,no_subtree_check,no_root_squash)

создать папку в /srv
chmod 777 /srv/homes
на клиенте установить nfs-common
в /etc/fstab на клиенте br-srv1.rea2023.ru:/srv/homes /home nfs defaults 0 0
mount /home - проверка
exportfs -a


7. файл из архива докер
поправить 11 строчку app.html
12 строчка --host=0.0.0.0
поругается на 4ю строчку возможно

docker build . -t app:latest
-делаем из папки с распак приложением
docker images
docker run --name=app -p 5000:5000 -d app
docker ps -a

curl localhost:5000


8. ЦС
apt install easy-rsa
cd /usr/share/easy-rsa/
cp vars.example vars
nano vars -> set_var easyrsa_pki "/etc/ca"
export vars
./easy-rsa init-pki
./easy-rsa build-ca nopass
./easy-rsa build-server -full web.rea2023.ru nopass
./easy-rsa таб 2 раза

9. docker wireguard
apt install docker-compose
mkdir -p ~/wireguard/config
vim ~/wireguard/docker-compose.yml
---
version: "2.1"
services:
  wireguard:
    image: lscr.io/linuxserver/wireguard
    container_name: wireguard
    cap_add:
      - NET_ADMIN
      - SYS_MODULE
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - SERVERURL=auto #optional
      - SERVERPORT=51820 #optional
      - PEERS=4 #optional
      - PEERDNS=auto #optional
      - INTERNAL_SUBNET=10.13.13.0 #optional
      - ALLOWEDIPS=0.0.0.0/0 #optional
    volumes:
      - ~/wireguard/config:/config
      - /lib/modules:/lib/modules
    ports:
      - 51820:51820/udp
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
    restart: unless-stopped

Создадим и запустим контейнер

cd ~/wireguard
docker-compose up -d

Дождемся пока скачается образ и запустится контейнер, после проверим, что он запущен командой docker ps. При успехе должны увидеть в выводе что - то вроде
ls -la ~/wireguard/config

Внутри каждой директории под каждого пира лежат заранее сгенерированные при старте контейнера файлы, а именно:

    приватный и публичный ключ (privatekey-peer1, publickey-peer1)
    peer1.png - изображение с qr кодом подключения
    peer1.conf - целевой конфигурационный файл, который требуется на клиенте

ls -la ~/wireguard/config/peer1

клиент
apt install wireguard
После установки нам необходимо на клиентской машине создать конфигурационный файл подключения /etc/wireguard/wg0.conf с содержимым файла ~/wireguard/config/peer1/peer1.conf с сервера.

Для этого можно например вывести содержимое файла peer1.conf в консоль командой cat ~/wireguard/config/peer1/peer1.conf, и скопипастить содержимое в целевой файл на клиенте.

Но вместо этого можно скачать файл при помощи утилиты scp с сервера прямо в целевой файл следующей единственной командой:
sudo scp root@ip-адрес-сервера:~/wireguard/config/peer1/peer1.conf /etc/wireguard/wg0.conf

Запуск

Все готово, осталось лишь запустить клиент следующей командой:

wg-quick up wg0

Проверим, что все работает:

curl ifconfig.me
195.255.255.255

Если в выводе ip адрес вашего сервера, то все получилось!









1. Поднять сайт.
    sudo apt update
    apt install apache2
    Настройка брандмауэра
   
    systemctl status apache2
    Создание виртуальных хостов

        Создадим каталог для rea2023, в /var/www/
        sudo mkdir -p /var/www/rea2023.ru/html

        chmod 777 /var/www/rea2023.ru

        touch index.html

        nano index.html

        Для обслуживания этого контента апач необходимо создать файл вирт хоста.
        вместо /etc/apache2/sites-available/000-default.conf создадим новый файл
        /etc/apache2/sites-available/web.rea2023.ru.conf

        nano

        <VirtualHost *:80>
    ServerAdmin admin@example.com (?)
    ServerName web.rea2023.ru
    ServerAlias www.web.rea2023.ru
    DocumentRoot /var/www/web.rea2023.ru/html
    RedirectMatch 404 "."
    CustomLog ${APACHE_LOG_DIR}/web.rea2023.ru.access.log combined
ErrorLog ${APACHE_LOG_DIR}/web.rea2023.ru.error.log
AssignUserID webuser webuser

<Directory /var/www/web.rea2023.ru>
Options -Includes -Indexes -ExecCGI
</Directory>
</VirtualHost>

<VirtualHost *:443>
    ServerAdmin admin@example.com (?)
    ServerName rea2023.ru
    ServerAlias www.rea2023.ru
    DocumentRoot /var/www/rea2023.ru/html
    RedirectMatch 404 "."
</VirtualHost>

Активируем файл
    a2ensite web.rea2023.ru.conf
Отключим файл по умочланию
    a2dissite 000-default.conf
Проверим ошибки конфигурации
    apache2ctl configtest
Перезапуск apache2 для внесения изменений
http://rea2023.ru

Занести в днс - ip=web.rea2023.ru
ip=webiles.rea2023.ru
 По аналогии создаётся другая папка - другой сайт



 Задание - заполнен LVM - том

 система блочных у-в - lsblk
 для получения информации о логических томах выполните:
lvdisplay

Прежде всего создадим физические тома, будьте внимательны, это действие уничтожит существующую разметку и все данные на дисках будут потеряны:

pvcreate /dev/sdb /dev/sdd
Затем создадим группу томов:

vgcreate andrey-lvm-vg /dev/sdb /dev/sdd
Синтаксис команды очень прост: указываем имя группы томов и входящие в нее диски.

Теперь можно создавать логические тома. Начнем с самых простых - томов с линейным отображением. Это можно сделать разными методами, например, указав желаемый размер тома:

lvcreate -n myvolume1 -L 10G andrey-lvm-vg


Перемещаемый профиль пользователя

NFS
1. sudo apt-install nfs-kernel-server
2. создаем каталог, который в дальнейшем будем расшаривать и задаём права доступа
sudo mkdir /mnt/storage
$ sudo chmod 777 /mnt/storage/
3. разрешаем сетевой доступ к каталогу для опеределённого клиента
sudo nano /etc/exports
/mnt/storage      192.168.10.8(rw,sync,no_root_squash,no_subtree_check)
4. применяем настройки сетевого доступа
sudo exportfs -r
5. Проверяем

$ sudo systemctl status rpcbind nfs-server

$ sudo exportfs
/mnt/storage    192.168.10.8

на клиенте
Обновляем список пакетов

$ sudo apt update
Устанавливаем NFS-клиент

$ sudo apt install nfs-common
Запускаем службы

$ sudo systemctl start rpcbind
$ sudo systemctl enable rpcbind
Создаем точку монтирования

$ sudo mkdir /mnt/localstr
$ sudo chown user:user /mnt/localstr
Монтируем NFS-каталог

$ sudo mount -t nfs4 192.168.10.12:/mnt/storage /mnt/localstr
где:

192.168.10.12:/mnt/storage – адрес NFS-сервера
/mnt/localstr – локальная точка монтирования
Настраиваем автоматическое монтирование

$ sudo nano /etc/fstab
[...]
192.168.10.12:/mnt/storage    /mnt/localstp    nfs    defaults    0 0







#!/usr/sbin/nft -f
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
chain input {
type filter hook input priority 0;
policy drop;
iif lo accept;
iif ens4 accept;
iif wg0 accept;
iif tun0 accept;

ct state established,related accept;

udp dport {500, 51820, 4500} accept;
ip protocol {ah, esp} accept;
ip protocol icmp reject;
counter drop;
}
chain forward {
type filter hook forward priority 0;
policy drop;
iif lo accept;
iif ens4 accept;
iif wg0 accept;
iif tun0 accept;

ct state established,related accept;
tcp dport {22} ct state new accept;
}
chain output {
type filter hook output priority 0;
}

}
table ip nat {
chain prerouting {
type nat hook prerouting priority 0;
dnat tcp dport map { 2222 : 192.168.3.10 } : tcp dport map { 2222 : 22 }
}
chain postrouting {
type nat hook postrouting priority 0;
oif ens3 masquerade;
}
}
